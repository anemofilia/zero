# Enter modal command mode.
KBD_CMD=^

# Exit modal command mode.
KBD_CMD_EXIT=ESC

# Quit command. This is used to exit command modes that don't require prefix
# (e.g. "w r" used for resizing panes).
KBD_QUIT=q

# Enter copy mode.
KBD_COPY_MODE=c

# Paste buffer (e.g. from copy mode).
KBD_PASTE=y

# Open the tmux command prompt.
KBD_CMD_PROMPT=:

# Window command prefix.
KBD_WIN=w

## Select window n (window command alias for KBD_GOTO_WIN_n).
KBD_WIN_GOTO_0=0
KBD_WIN_GOTO_1=1
KBD_WIN_GOTO_2=2
KBD_WIN_GOTO_3=3
KBD_WIN_GOTO_4=4
KBD_WIN_GOTO_5=5
KBD_WIN_GOTO_6=6
KBD_WIN_GOTO_7=7
KBD_WIN_GOTO_8=8
KBD_WIN_GOTO_9=9

## Select window
KBD_WIN_GOTO_TREE=t    # with tree view
KBD_WIN_GOTO_INDEX=i   # with index

## Select pane.
KBD_WIN_PANE_LEFT=h
KBD_WIN_PANE_RIGHT=l
KBD_WIN_PANE_UP=k
KBD_WIN_PANE_DOWN=j

## Delete window pane.
KBD_WIN_PANE_DEL=d

## Select window
KBD_WIN_PREV=H    # previous
KBD_WIN_NEXT=L    # next
KBD_WIN_LAST=o    # last

## Delete window.
KBD_WIN_DEL=D

## Create new window.
KBD_WIN_CREATE=c

## Zoom pane.
KBD_WIN_ZOOM=z

## Break pane.
KBD_WIN_BREAK=b

## Display pane numbers.
KBD_WIN_NR=n

## Rename window.
KBD_WIN_RENAME=,

## Pane command prefix (same bindings as KBD_WIN but without the prefix).
KBD_WIN_PANE=w

## Split command prefix.
KBD_WIN_SPLIT=s

### Split window pane
KBD_WIN_SPLIT_RIGHT=l
KBD_WIN_SPLIT_DOWN=j

## Move command prefix.
KBD_WIN_MOVE=m

### Move window pane
KBD_WIN_MOVE_UP=k
KBD_WIN_MOVE_DOWN=j

## Arrange command prefix.
KBD_WIN_ARRANGE=a

### Arrange window layout
KBD_WIN_ARRANGE_1=1         # even-horizontal
KBD_WIN_ARRANGE_2=2         # even-vertical
KBD_WIN_ARRANGE_3=3         # main-horizontal
KBD_WIN_ARRANGE_4=4         # main-vertical

##q Resize command prefix.
KBD_WIN_RESIZE=r

### Resize pane one step.
KBD_WIN_RESIZE_LEFT=h
KBD_WIN_RESIZE_RIGHT=l      
KBD_WIN_RESIZE_DOWN=j
KBD_WIN_RESIZE_UP=k

### Resize pane multiple steps.
KBD_WIN_RESIZE_MULTI_LEFT=H
KBD_WIN_RESIZE_MULTI_RIGHT=L
KBD_WIN_RESIZE_MULTI_DOWN=J
KBD_WIN_RESIZE_MULTI_UP=K

# Session command prefix.
KBD_SESS=s

## Detach session.
KBD_SESS_DETACH=d

## Select session
KBD_SESS_PREV=h     # previous
KBD_SESS_NEXT=l     # next
KBD_SESS_TREE=t     # with tree view

## Delete session.
KBD_SESS_DEL=D

# "Go to" command prefix.
KBD_GOTO=g

## Go to window command prefix.
KBD_GOTO_WIN=w

### Go to window n.
KBD_GOTO_WIN_0=0
KBD_GOTO_WIN_1=1
KBD_GOTO_WIN_2=2
KBD_GOTO_WIN_3=3
KBD_GOTO_WIN_4=4
KBD_GOTO_WIN_5=5
KBD_GOTO_WIN_6=6
KBD_GOTO_WIN_7=7
KBD_GOTO_WIN_8=8
KBD_GOTO_WIN_9=9

### Go to window.
KBD_GOTO_WIN_TREE=t    # with tree view
KBD_GOTO_WIN_INDEX=i   # with index
KBD_GOTO_WIN_PREV=h    # previous
KBD_GOTO_WIN_NEXT=l    # next
KBD_GOTO_WIN_LAST=o    # last

### Go to session.
KBD_GOTO_SESS_PREV=h    # previous
KBD_GOTO_SESS_NEXT=l    # next
KBD_GOTO_SESS_TREE=t    # with tree view
